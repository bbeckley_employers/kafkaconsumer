package com.kafka.consumer.consumer.kafka;

import io.confluent.examples.clients.cloud.DataRecordAvro;
import lombok.extern.log4j.Log4j2;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class ConsumerExample {

  @KafkaListener(topics = "${com.employers.inc.config.topic.name}")
  public void consume(final ConsumerRecord<Long, DataRecordAvro> consumerRecord) {
    log.info("received {} {}", consumerRecord.key(), consumerRecord.value());

  }
}
